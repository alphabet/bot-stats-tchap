# Bot Stats Tchap

This bot is built using the Simple-Matrix-Bot-Lib library(https://github.com/i10b/simplematrixbotlib), which is a Python bot library for the Matrix ecosystem built on matrix-nio(https://github.com/poljar/matrix-nio). It provides a streamlined approach to building Matrix bots with minimal code.

This bot extends the functionality of the Simple-Matrix-Bot-Lib library by adding custom features for specific use cases. It allows you to interact with Metabase questions and send the results to groups or channels.

Avant de démarrer le bot effacez fonctionalités/store/ et fonctionalités/session.txt pour que le bot commence plus rapidement.  
```

cd fonctionnalités

python3 -m venv venv

source venv/bin/activate

pip install wheel

pip install importlib-metadata==4.13.0

pip install schedule

pip install apscheduler

python3 -m pip install simplematrixbotlib

python -m pip install "matrix-nio[e2e]"

pip install requests

python3 fonctionalite_souhate.py



## Example Usage

```python
# data.py
# Example:
# randomuser - "!data tubes"
# data_bot - "Nombre de videos publiees sur peertube: 123456789"

