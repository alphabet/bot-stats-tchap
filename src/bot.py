import asyncio
import simplematrixbotlib as botlib
import requests
import time
import schedule 
from apscheduler.schedulers.asyncio import AsyncIOScheduler
import datetime
import os 
from config import (
    send_time_hour, send_time_minute,
    bot_name, bot_password, bot_homeserver,
    command_filters, room_filters,
    base_url, dashboard_id, metabase_username, metabase_password
)


creds = botlib.Creds(
    bot_homeserver,
    bot_name,
    bot_password
)
bot = botlib.Bot(creds)
PREFIX = '!'

def get_metabase_session_token():
    response = requests.post(
        f'{base_url}/api/session',
        json={'username': metabase_username , 'password': metabase_password } 
    )
    response.raise_for_status()
    return response.json()['id']

def get_matrix_session_token(bot_name, bot_password, homeserver):
    login_url = f"{homeserver}/_matrix/client/r0/login"
    data = {
        "type": "m.login.password",
        "identifier": {
            "type": "m.id.user",
            "user": bot_name
        },
        "password": bot_password
    }

    try:
        response = requests.post(login_url, json=data)
        response.raise_for_status()
        session_token = response.json().get("access_token")
        return session_token
    except requests.exceptions.RequestException as e:
        print(f"An error occurred while getting the session token: {e}")
        return None


def get_joined_room_id(session_token):
    url = f"{bot_homeserver}/_matrix/client/v3/joined_rooms"
    headers = {'Accept': 'application/json'}
    params = {'access_token': session_token}

    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        joined_rooms = data.get('joined_rooms', [])
        return joined_rooms
    else:
        print(f"Request error: {response.status_code}")
        return []


def get_room_names(room_id, session_token):
    url = f"{bot_homeserver}/_matrix/client/v3/rooms/{room_id}/aliases"
    headers = {"Authorization": f"Bearer {session_token}", "Accept": "application/json"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()
        data = response.json()
        aliases = data.get("aliases", [])
        return aliases[0] if aliases else None
    except requests.exceptions.RequestException as e:
        print(f"Error fetching room aliases for {room_id}: {e}")
        return None


def get_dashboard_data(session_token):
    url = f'{base_url}/api/dashboard/{dashboard_id}'
    response = requests.get(
        url,
        headers={'X-Metabase-Session': session_token}
    )
    response.raise_for_status()
    return response.json()


def extract_question_data(dashboard_data, filters):
    question_data = []
    cards = dashboard_data.get('ordered_cards', [])
    for card in cards:
        card_info = card.get('card')
        question_id = card_info.get('id')
        question_title = card_info.get('name')
        if question_id is not None and question_title is not None:
            for filter_value in filters:
                if filter_value in question_title:
                    question_data.append((question_title, question_id))
                    break
    return question_data

def get_question_count(session_token, question_id):
    response = requests.get(
        f'{base_url}/api/card/{question_id}',
        headers={'X-Metabase-Session': session_token}
    )
    try:
        data = response.json()
        count = int(data['result_metadata'][0]['fingerprint']['type']['type/Number']['avg'])
        return count
    except (KeyError, IndexError, ValueError):
        return None
        

@bot.listener.on_message_event
async def handle_message(room, message):
    match = botlib.MessageMatch(room, message, bot, PREFIX)

    if match.is_not_from_this_bot() and match.prefix():
        content = message.body.strip()
        if content == "!data help":
            await bot.api.send_text_message(room.room_id, "Bonjour, je suis en bot qui vous envoie des data concernant les services de apps. Voici les commandes auxquels je réponds.\n!data apps\n!data visio\n!data tubes\n!data codimd et !data help")
        else:
            if content in command_filters:
                command = content
                filters = command_filters[command]

                session_token = get_metabase_session_token()
                dashboard_data = get_dashboard_data(session_token)

                question_data = extract_question_data(dashboard_data, filters)

                answers = []
                for question_title, question_id in question_data:
                    count = get_question_count(session_token, question_id)
                    if count is not None:
                        answers.append(f"{question_title}: {count}")

                if len(answers) > 0:
                    await bot.api.send_text_message(room.room_id, '\n'.join(answers))
                    print(f"Message sent to room {room.room_id}")


scheduler = AsyncIOScheduler()

def schedule_auto_messages():
    scheduler = AsyncIOScheduler()
    scheduler.add_job(send_auto_messages, 'cron', hour=send_time_hour, minute=send_time_minute)
    scheduler.start()


async def send_auto_messages():
    await auto_messages()

async def auto_messages():
    print("called function")
    metabase_session_token = get_metabase_session_token()
    matrix_session_token = get_matrix_session_token(bot_name, bot_password, bot_homeserver)
    dashboard_data = get_dashboard_data(metabase_session_token)
    joined_rooms = get_joined_room_id(matrix_session_token)

    print(f"Joined Rooms: {joined_rooms}")

    for joined_room_id in joined_rooms:
        room_name = get_room_names(joined_room_id, matrix_session_token)
        if room_name:
            print(f"Room ID: {joined_room_id}, Room Name: {room_name}")

            for command, filters in room_filters.items():
                if command in room_name:
                    print(f"Room matches filter: {command}")

                    question_data = extract_question_data(dashboard_data, filters)

                    answers = []
                    for question_title, question_id in question_data:
                        count = get_question_count(metabase_session_token, question_id)
                        if count is not None:
                            answers.append(f"{question_title}: {count}")

                    if len(answers) > 0:
                        try:
                            print("Sending message...")
                            await bot.api.send_text_message(joined_room_id, f"!data {command}\n" + '\n'.join(answers))
                            print("Message sent!")
                        except Exception as e:
                            print(f"Error sending message: {e}")


if __name__ == "__main__":
    os.system("rm session.txt | rm -rf store")
    schedule_auto_messages()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(bot.main())
    loop.close()
