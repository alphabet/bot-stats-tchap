#general
send_time_hour = X
send_time_minute = X

#bot
bot_name = "bot_name"
bot_password = "bot_password"
bot_homeserver = "bot_homeserver"

#Command filters
command_filters = {
    "!data X": ["filter_X", "filter_X"],
    "!data Y": ["filter_X"],
    "!data Z": ["filter_X", "filter_X"],
    "!data Q": ["filter_X"]
}

#Room filters
room_filters = {
    "!room_id": ["filter_X","filter_X", "filter_X"],
    # ... other room filters ...
}

#Metabase settings
base_url = 'metabase_url'
dashboard_id = X
metabase_username = 'metabase_username'
metabase_password = 'metabase_password'